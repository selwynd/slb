# SLB

My name is Selwyn Dijkstra I'm a student at the Institute for life science and technology studying bio-informatics.
Projects centered around healthcare are most interessting to me. If a finished product can be used to improve diagnose or treat people that gives me the greatest motivation.
For instance I build a pipeline where when you input a human DNA sample the results will be a report of known pathogen presence inside the sample. (https://bitbucket.org/selwynd/data-proccesing/src/master/Eindopdracht/)
Also projects which consist mostly of programming with a bit of biological knowledge interest me. In one project I tried to use image processing to make a biological analysis.  (https://bitbucket.org/flimsi/flim/src/master/) 
My latest project is about audio processing of sounds from a bee hive. This project also is very interesting to me because I learn different analysis techniques on different types of files. (https://bitbucket.org/thebeegees/beeaudioclassification/src/master/)
